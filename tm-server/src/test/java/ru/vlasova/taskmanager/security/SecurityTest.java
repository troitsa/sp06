package ru.vlasova.taskmanager.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.config.TestContext;
import ru.vlasova.taskmanager.config.WebConfig;
import ru.vlasova.taskmanager.model.entity.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebConfig.class})
public class SecurityTest {

    private MockMvc mockMvc;

    private Principal mockPrincipal = mock(Principal.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private IUserService userService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void getUser() throws Exception {
        User user = new User();
        List<User> users = new ArrayList<>();
        users.add(user);
        when(userService.findUserById(any()))
                .thenReturn(user);
        mockMvc.perform(get("/admin/get/{userId}", user.getId())
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("admin"))
                .andExpect(forwardedUrl("/WEB-INF/view/admin.jsp"))
                .andExpect(model().attribute("user", hasProperty("id", is(user.getId()))));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void dontGetUser() throws Exception {
        mockMvc.perform(get("/admin/get/{userId}", "test")
                .principal(mockPrincipal))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithAnonymousUser
    public void projectList() throws Exception {
        mockMvc.perform(get("/project_list").principal(mockPrincipal))
                .andExpect(status().is4xxClientError());
    }

}
