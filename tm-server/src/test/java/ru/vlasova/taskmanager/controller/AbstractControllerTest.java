package ru.vlasova.taskmanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.vlasova.taskmanager.config.ApplicationConfig;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringJUnitConfig(ApplicationConfig.class)
public class AbstractControllerTest {

    protected Project newProject(@NotNull final String s) {
        @NotNull final Project project = new Project();
        project.setId("test");
        project.setName("TestProject" + s);
        project.setDescription("TestDescription");
        project.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        project.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return project;
    }

    private Date toDateFormat(@NotNull final String date) {
        TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(date);
        Instant i = Instant.from(ta);
        return Date.from(i);
    }

    protected static User newUser(@NotNull final String s) {
        @NotNull final User user = new User();
        user.setId("test");
        user.setUsername("test" + s);
        user.setPassword("pass");
        return user;
    }

    protected Task newTask(@NotNull final String s) {
        @NotNull final Task task = new Task();
        task.setName("TestTask" + s);
        task.setDescription("Description 123");
        task.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        task.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return task;
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
