package ru.vlasova.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.config.TestContext;
import ru.vlasova.taskmanager.config.WebConfig;
import ru.vlasova.taskmanager.model.entity.Role;
import ru.vlasova.taskmanager.model.entity.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.vlasova.taskmanager.enumeration.RoleType.ADMIN;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebConfig.class})
public class UserControllerTest extends AbstractControllerTest {

    private MockMvc mockMvc;

    private User admin;

    @Autowired
    private IUserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Principal mockPrincipal = mock(Principal.class);

    @Before
    public void setUp() {
        admin = newUser("");
        @NotNull final List<Role> roles = new ArrayList<>();
        @NotNull final Role role = new Role();
        role.setRole(ADMIN);
        roles.add(role);
        admin.setRoles(roles);
        userService.saveUser(admin);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
        when(userService.findByUsername("admin")).thenReturn(admin);
    }

    @Test
    public void registration() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(view().name("registration"));

    }

    @Test
    public void addUser() throws Exception {
        when(userService.saveUser(ArgumentMatchers.any(User.class)))
                .thenReturn(true);
        mockMvc.perform(post("/registration")
                .content(asJsonString(admin))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(view().name("redirect:login"));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void userList() throws Exception {
        User user = newUser("");
        List<User> users = new ArrayList<>();
        users.add(user);
        when(userService.allUsers())
                .thenReturn(users);
        mockMvc.perform(get("/admin").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("admin"))
                .andExpect(forwardedUrl("/WEB-INF/view/admin.jsp"))
                .andExpect(model().attribute("allUsers", hasSize(1)))
                .andExpect(model().attribute("allUsers", hasItem(
                        allOf(
                                hasProperty("id", is(user.getId())),
                                hasProperty("username", is(user.getUsername()))
                        )
                )));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void deleteUser() throws Exception {
        mockMvc.perform(post("/admin/delete")
                .param("userId", "test")
                .param("action", "delete")
                .principal(mockPrincipal))
                .andExpect(view().name("redirect:/admin"));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void getUser() throws Exception {
        User user = newUser("");
        List<User> users = new ArrayList<>();
        users.add(user);
        when(userService.findUserById(any()))
                .thenReturn(user);
        mockMvc.perform(get("/admin/get/{userId}", user.getId())
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("admin"))
                .andExpect(forwardedUrl("/WEB-INF/view/admin.jsp"))
                .andExpect(model().attribute("user", hasProperty("id", is(user.getId()))));
    }
}
