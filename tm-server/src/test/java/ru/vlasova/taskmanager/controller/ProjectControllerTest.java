package ru.vlasova.taskmanager.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.config.TestContext;
import ru.vlasova.taskmanager.config.WebConfig;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebConfig.class})
public class ProjectControllerTest extends AbstractControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Principal mockPrincipal = mock(Principal.class);

    private static User user;
    private Project project1 = newProject("");

    @Before
    public void setUp() {
        user = newUser("");
        userService.saveUser(user);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
        when(mockPrincipal.getName()).thenReturn("test");
        when(userService.findByUsername("test")).thenReturn(user);
    }

    @After
    public void after() {
        userService.deleteUser(user.getId());
    }

    @Test
    @WithMockUser("test")
    public void projectList() throws Exception {
        List<Project> projects = new ArrayList<>();
        projects.add(project1);
        when(projectService.findAllByUserId(user.getId()))
                .thenReturn(projects);
        mockMvc.perform(get("/project_list").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("project_list"))
                .andExpect(forwardedUrl("/WEB-INF/view/project_list.jsp"))
                .andExpect(model().attribute("projectList", hasSize(1)))
                .andExpect(model().attribute("projectList", hasItem(
                        allOf(
                                hasProperty("id", is(project1.getId())),
                                hasProperty("description", is(project1.getDescription())),
                                hasProperty("name", is(project1.getName()))
                        )
                )));
    }

    @Test
    @WithMockUser("test")
    public void newProjectForm() throws Exception {
        mockMvc.perform(get("/new_project").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("new_project"))
                .andExpect(forwardedUrl("/WEB-INF/view/new_project.jsp"))
                .andExpect(model().attribute("project", hasProperty("id")));
    }

    @Test
    @WithMockUser("test")
    public void saveProject() throws Exception {
        mockMvc.perform(post("/project_save")
                .content(asJsonString(project1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(view().name("redirect:/project_list"));
    }

    @Test
    @WithMockUser("test")
    public void editProjectForm() throws Exception {
        when(projectService.findOne(project1.getId(), user.getId()))
                .thenReturn(project1);
        mockMvc.perform(get("/edit_project")
                .param("id", project1.getId())
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("edit_project"))
                .andExpect(forwardedUrl("/WEB-INF/view/edit_project.jsp"));
    }

    @Test
    @WithMockUser("test")
    public void deleteProjectForm() throws Exception {
        mockMvc.perform(post("/delete_project")
                        .param("id", project1.getId())
                        .principal(mockPrincipal))
                .andExpect(view().name("redirect:/project_list"));
    }

}
