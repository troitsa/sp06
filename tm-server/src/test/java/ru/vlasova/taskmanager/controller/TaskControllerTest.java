package ru.vlasova.taskmanager.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.config.TestContext;
import ru.vlasova.taskmanager.config.WebConfig;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebConfig.class})
public class TaskControllerTest extends AbstractControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Principal mockPrincipal = mock(Principal.class);

    private static User user;
    private Task task1 = newTask("");

    @Before
    public void setUp() {
        user = newUser("");
        userService.saveUser(user);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
        when(mockPrincipal.getName()).thenReturn("test");
        when(userService.findByUsername("test")).thenReturn(user);
    }

    @After
    public void after() {
        userService.deleteUser(user.getId());
    }

    @Test
    @WithMockUser("test")
    public void taskList() throws Exception {
        List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        when(taskService.findAll())
                .thenReturn(tasks);
        mockMvc.perform(get("/task_list").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("task_list"))
                .andExpect(forwardedUrl("/WEB-INF/view/task_list.jsp"))
                .andExpect(model().attribute("taskList", hasSize(1)))
                .andExpect(model().attribute("taskList", hasItem(
                        allOf(
                                hasProperty("id", is(task1.getId())),
                                hasProperty("description", is(task1.getDescription())),
                                hasProperty("name", is(task1.getName()))
                        )
                )));
    }

    @Test
    @WithMockUser("test")
    public void newTaskForm() throws Exception {
        mockMvc.perform(get("/new_task").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("new_task"))
                .andExpect(forwardedUrl("/WEB-INF/view/new_task.jsp"))
                .andExpect(model().attribute("task", hasProperty("id")));
    }

    @Test
    @WithMockUser("test")
    public void saveTask() throws Exception {
        mockMvc.perform(post("/task_save")
                .content(asJsonString(task1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(view().name("redirect:/task_list"));
    }

    @Test
    @WithMockUser("test")
    public void editTaskForm() throws Exception {
        when(taskService.findOne(any(), any()))
                .thenReturn(task1);
        mockMvc.perform(get("/edit_task")
                .param("id", task1.getId())
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("edit_task"))
                .andExpect(forwardedUrl("/WEB-INF/view/edit_task.jsp"));
    }

    @Test
    @WithMockUser("test")
    public void deleteTaskForm() throws Exception {
        mockMvc.perform(post("/delete_task")
                .param("id", task1.getId())
                .principal(mockPrincipal))
                .andExpect(view().name("redirect:/task_list"));
    }

}
