package ru.vlasova.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.List;

import static org.junit.Assert.*;

public class TaskRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    @Qualifier(value = "userRepository")
    private UserRepository userRepository;

    @Autowired
    @Qualifier(value = "taskRepository")
    private TaskRepository taskRepository;

    @Test
    public void createTaskTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        task1.setUser(user);
        taskRepository.save(task1);
        assertNotNull(taskRepository.findById(task1.getId()));
        taskRepository.delete(task1);
        userRepository.delete(user);
    }

    @Test
    public void findAllTaskByUserIdTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        task1.setUser(user);
        taskRepository.save(task1);
        assertTrue(taskRepository.findAllByUserId(user.getId()).contains(task1));
        taskRepository.delete(task1);
        userRepository.delete(user);
    }

    @Test
    public void findTaskTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        task1.setUser(user);
        taskRepository.save(task1);
        assertEquals(taskRepository.findById(task1.getId()).orElse(null),task1);
        taskRepository.delete(task1);
        userRepository.delete(user);
    }

    @Test
    public void findAllTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        @NotNull final Task task2 = newTask("2");
        task1.setUser(user);
        task2.setUser(user);
        taskRepository.save(task1);
        taskRepository.save(task2);
        List<Task> tasks = (List<Task>) taskRepository.findAll();
        assertTrue(tasks.size()>0);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskRepository.delete(task1);
        taskRepository.delete(task2);
        userRepository.delete(user);
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        @NotNull final Task task2 = newTask("2");
        task1.setUser(user);
        task2.setUser(user);
        taskRepository.save(task1);
        taskRepository.save(task2);
        List<Task> tasks = taskRepository.findAllByUserId(user.getId());
        assertTrue(tasks.size() == 2);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskRepository.delete(task1);
        taskRepository.delete(task2);
        userRepository.delete(user);
    }

    @Test
    public void updateTaskTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        task1.setUser(user);
        taskRepository.save(task1);
        assertEquals(taskRepository.findById(task1.getId()).orElse(null),task1);
        task1.setName("Tesst");
        taskRepository.save(task1);
        assertEquals(taskRepository.findById(task1.getId()).orElse(null),task1);
        taskRepository.delete(task1);
        userRepository.delete(user);
    }

    @Test
    public void removeTaskByIdTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask("1");
        task1.setUser(user);
        taskRepository.save(task1);
        assertEquals(taskRepository.findById(task1.getId()).orElse(null),task1);
        taskRepository.delete(task1);
        @Nullable final Task task2 = taskRepository.findById(task1.getId()).orElse(null);
        assertNull(task2);
        userRepository.delete(user);
    }

    @Test
    public void findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCaseTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Task task1 = newTask(" lloorreemm");
        @NotNull final Task task2 = newTask(" 2  lloorreemm");
        task1.setUser(user);
        task2.setUser(user);
        task1.setDescription(" lloorreemm 1");
        taskRepository.save(task1);
        taskRepository.save(task2);
        List<Task> tasks = taskRepository
                .findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(user.getId(),
                        "lloorreemm",
                        "lloorreemm");
        assertTrue(tasks.size() == 2);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskRepository.delete(task1);
        taskRepository.delete(task2);
        userRepository.delete(user);
    }

    private Task newTask(@NotNull final String s) {
        @NotNull final Task task = new Task();
        task.setName("TestTask" + s);
        task.setDescription("Description 123");
        task.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        task.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return task;
    }

}
