package ru.vlasova.taskmanager.marshalling;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.Date;

import static org.junit.Assert.assertEquals;


public class MarshallingTest {

    @Test
    public void marshallingProjectTest() throws Exception {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId("userId");
        projectDTO.setName("test");
        projectDTO.setDescription("description");
        projectDTO.setStatus(Status.PLANNED);
        projectDTO.setDateStart(new Date(System.currentTimeMillis()));
        projectDTO.setDateFinish(new Date(System.currentTimeMillis()));
        @NotNull final String json = asJsonString(projectDTO);
        @NotNull final ProjectDTO result = new ObjectMapper().readValue(json, ProjectDTO.class);
        assertEquals(projectDTO.getId(), result.getId());
        assertEquals(projectDTO.getDescription(), result.getDescription());
        assertEquals(projectDTO.getDateStart(), result.getDateStart());
        assertEquals(projectDTO.getDateFinish(), result.getDateFinish());
        assertEquals(projectDTO.getStatus(), result.getStatus());
        assertEquals(projectDTO.getName(), result.getName());
        assertEquals(projectDTO.getDateCreate(), result.getDateCreate());
        assertEquals(projectDTO.getUserId(), result.getUserId());
    }

    @Test
    public void marshallingTaskTest() throws Exception {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId("userId");
        taskDTO.setName("test");
        taskDTO.setDescription("description");
        taskDTO.setStatus(Status.PLANNED);
        taskDTO.setDateStart(new Date(System.currentTimeMillis()));
        taskDTO.setDateFinish(new Date(System.currentTimeMillis()));
        @NotNull final String json = asJsonString(taskDTO);
        @NotNull final TaskDTO result = new ObjectMapper().readValue(json, TaskDTO.class);
        assertEquals(taskDTO.getId(), result.getId());
        assertEquals(taskDTO.getDescription(), result.getDescription());
        assertEquals(taskDTO.getDateStart(), result.getDateStart());
        assertEquals(taskDTO.getDateFinish(), result.getDateFinish());
        assertEquals(taskDTO.getStatus(), result.getStatus());
        assertEquals(taskDTO.getName(), result.getName());
        assertEquals(taskDTO.getDateCreate(), result.getDateCreate());
        assertEquals(taskDTO.getUserId(), result.getUserId());
    }

    @Test
    public void marshallingUserTest() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId("userId");
        userDTO.setUsername("test");
        userDTO.setPassword("password");
        @NotNull final String json = asJsonString(userDTO);
        @NotNull final UserDTO result = new ObjectMapper().readValue(json, UserDTO.class);
        assertEquals(userDTO.getId(), result.getId());
        assertEquals(userDTO.getUsername(), result.getUsername());
        assertEquals(userDTO.getPassword(), result.getPassword());
    }

    private String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
