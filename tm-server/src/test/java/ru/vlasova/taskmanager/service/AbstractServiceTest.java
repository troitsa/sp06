package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.vlasova.taskmanager.config.ApplicationConfig;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;


@RunWith(SpringRunner.class)
@SpringJUnitConfig(ApplicationConfig.class)
public class AbstractServiceTest {
    protected Project newProject(@NotNull final String s) {
        @NotNull final Project project = new Project();
        project.setName("TestProject" + s);
        project.setDescription("Description 123");
        project.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        project.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return project;
    }

    private Date toDateFormat(@NotNull final String date) {
        TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(date);
        Instant i = Instant.from(ta);
        return Date.from(i);
    }

    protected static User newUser(@NotNull final String s) {
        @NotNull final User user = new User();
        user.setUsername("userTest" + s);
        user.setPassword("passTest");
        return user;
    }

    protected Task newTask(@NotNull final String s) {
        @NotNull final Task task = new Task();
        task.setName("TestTask" + s);
        task.setDescription("Description 123");
        task.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        task.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return task;
    }
}
