package ru.vlasova.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.taskmanager.model.entity.Project;

import java.util.List;

@Repository("projectRepository")
public interface ProjectRepository extends CrudRepository<Project, String> {

    List<Project> findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String userId, String searchString1, String searchString2);

    List<Project> findAllByUserIdOrderByDateCreate(String userId);

    List<Project> findAllByUserIdOrderByDateStart(String userId);

    List<Project> findAllByUserIdOrderByDateFinish(String userId);

    List<Project> findAllByUserIdOrderByStatusAsc(String userId);

    List<Project> findAllByUserIdOrderByNameAsc(String userId);

    List<Project> findAllByUserId(String userId);

    Project findByIdAndUserId(String id, String userId);
}
