package ru.vlasova.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.entity.User;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("registration")
    public String registration(@NotNull final Model model) {
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping("registration")
    public String addUser(@NotNull @ModelAttribute("user") @Valid final User user,
                          @NotNull final BindingResult bindingResult,
                          @NotNull final Model model) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (!userService.saveUser(user)){
            model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
            return "registration";
        }
        return "redirect:login";
    }

    @GetMapping("/admin")
    @Secured({"ROLE_ADMIN"})
    public String userList(@NotNull final Model model) {
        model.addAttribute("allUsers", userService.allUsers());
        return "admin";
    }

    @PostMapping("/admin/delete")
    @Secured({"ROLE_ADMIN"})
    public String deleteUser(@NotNull @RequestParam final String userId,
                              @NotNull @RequestParam final String action,
                              @NotNull final Model model) {
        if (action.equals("delete")){
            userService.deleteUser(userId);
        }
        return "redirect:/admin";
    }

    @GetMapping("/admin/get/{userId}")
    @Secured({"ROLE_ADMIN"})
    public String  getUser(@NotNull @PathVariable("userId") final String userId,
                           @NotNull final Model model) {
        model.addAttribute("user", userService.findUserById(userId));
        return "admin";
    }

}
