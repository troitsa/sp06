package ru.vlasova.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class TaskController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    @Autowired
    public TaskController(ITaskService taskService, IProjectService projectService, IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @RequestMapping("/task_list")
    public ModelAndView taskList() {
        @Nullable final List<Task> taskList = taskService.findAll();
        @NotNull final ModelAndView mav = new ModelAndView("task_list");
        mav.addObject("taskList", taskList);
        return mav;
    }

    @RequestMapping("/new_task")
    public String newTaskForm(@NotNull final Map<String, Object> model) {
        @NotNull final TaskDTO task = new TaskDTO();
        @Nullable final List<ProjectDTO> projectList = projectService
                .findAllByUserId(getCurrentUserId())
                .stream()
                .map(projectService::toProjectDTO)
                .collect(Collectors.toList());
        model.put("task", task);
        model.put("projectList", projectList);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        model.put("statusList", statusList);
        return "new_task";
    }

    @RequestMapping(value = "task_save", method = RequestMethod.POST)
    public String saveTask(@Nullable @ModelAttribute final TaskDTO task) {
        task.setUserId(getCurrentUserId());
        taskService.merge(taskService.toTask(task));
        return "redirect:/task_list";
    }

    @RequestMapping("/edit_task")
    public ModelAndView editTaskForm(@NotNull @RequestParam final String id) {
        ModelAndView mav = new ModelAndView("edit_task");
        @Nullable final TaskDTO task = taskService.toTaskDTO(taskService.findOne(id, getCurrentUserId()));
        @Nullable final List<ProjectDTO> projectList = projectService
                .findAllByUserId(getCurrentUserId())
                .stream()
                .map(projectService::toProjectDTO)
                .collect(Collectors.toList());
        mav.addObject("task", task);
        mav.addObject("projectList", projectList);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        mav.addObject("statusList", statusList);
        return mav;
    }

    @RequestMapping("/delete_task")
    public String deleteTaskForm(@NotNull @RequestParam final String id) {
        taskService.remove(id);
        return "redirect:/task_list";
    }

    @RequestMapping("search_task")
    public ModelAndView search(@NotNull @RequestParam final String keyword) {
        @Nullable final List<Task> result = taskService.search(keyword, getCurrentUserId());
        @NotNull final ModelAndView mav = new ModelAndView("search_task");
        mav.addObject("result", result);
        return mav;
    }

    private String getCurrentUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findByUsername(auth.getName()).getId();
    }
}
