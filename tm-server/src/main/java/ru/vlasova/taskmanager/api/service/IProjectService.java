package ru.vlasova.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.Task;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project insert(@Nullable final String name, @Nullable final String userId,
                   @Nullable final String description, @Nullable final Date dateStart,
                   @Nullable final Date dateFinish, @Nullable final String status);

    void remove(@Nullable final String id);

    @Nullable
    List<Project> search(@Nullable final String searchString, @Nullable final String userId);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOne(@Nullable final String id, @Nullable final String userId);

    void persist(@Nullable final Project obj);

    void merge(@Nullable final Project obj);

    void removeAll();

    @Nullable
    List<Project> sortProject(@Nullable final String sortMode, @Nullable final String userId);

    @Nullable
    Project toProject(@Nullable final ProjectDTO projectDTO);

    @Nullable
    ProjectDTO toProjectDTO(@Nullable final Project project);

    @NotNull
    List<Project> findAllByUserId(@Nullable final String userId);
}