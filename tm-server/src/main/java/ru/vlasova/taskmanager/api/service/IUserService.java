package ru.vlasova.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findUserById(@Nullable final String userId);

    @Nullable
    List<User> allUsers();

    boolean saveUser(@Nullable final User user);

    boolean deleteUser(@Nullable final String userId);

    User findByUsername(@Nullable final String username);

    @Nullable
    User toUser(@Nullable final UserDTO userDTO);

    @Nullable
    UserDTO toUserDTO(@Nullable final User user);

}
