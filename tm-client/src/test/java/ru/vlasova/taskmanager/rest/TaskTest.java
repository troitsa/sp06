package ru.vlasova.taskmanager.rest;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.List;

public class TaskTest extends AbstractTest {

    private static UserDTO user;
    private static String auth;

    @BeforeClass
    public static void before() {
        user = newUser("1");
        userClient.createUser(user);
        auth = login(user.getUsername(), user.getPassword());
    }

    @AfterClass
    public static void after() {
        userClient.deleteUser(auth, user.getId());
    }

    @Test
    public void createTask() {
        @NotNull final TaskDTO task = newTask("1");
        task.setUserId(user.getId());
        taskClient.createTask(auth, task);
        Assert.assertNotNull(taskClient.getTask(auth, task.getId()));
        taskClient.deleteTask(auth, task.getId());
    }

    @Test
    public void getTasks() {
        @NotNull final TaskDTO task1 = newTask("1");
        @NotNull final TaskDTO task2 = newTask("2");
        @NotNull final TaskDTO task3 = newTask("2");
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        task3.setUserId(user.getId());
        taskClient.createTask(auth, task1);
        taskClient.createTask(auth, task2);
        taskClient.createTask(auth, task3);
        @NotNull final List<TaskDTO> taskList = taskClient.getTasks(auth);
        Assert.assertEquals(taskList.size(), 2);
        taskClient.deleteTask(auth, task1.getId());
        taskClient.deleteTask(auth, task2.getId());
        taskClient.deleteTask(auth, task3.getId());
    }

    @Test
    public void getTask() {
        @NotNull final TaskDTO task = newTask("1");
        task.setUserId(user.getId());
        taskClient.createTask(auth, task);
        Assert.assertEquals(taskClient.getTask(auth, task.getId()), task);
        taskClient.deleteTask(auth, task.getId());
    }

    @Test
    public void updateTask() {
        @NotNull final TaskDTO task = newTask("1");
        task.setUserId(user.getId());
        taskClient.createTask(auth, task);
        task.setName("Test-2");
        taskClient.updateTask(auth, task.getId(), task);
        Assert.assertEquals(taskClient.getTask(auth, task.getId()).getName(), task.getName());
        taskClient.deleteTask(auth, task.getId());
    }

    @Test
    public void deleteTask() {
        @NotNull final TaskDTO task = newTask("1");
        task.setUserId(user.getId());
        taskClient.createTask(auth, task);
        taskClient.deleteTask(auth, task.getId());
        try {
            taskClient.getTask(auth, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("404"));
        }
    }

    @Test
    public void searchTasks() {
        @NotNull final TaskDTO task1 = newTask("1");
        task1.setName("tasksk");
        task1.setUserId(user.getId());
        @NotNull final TaskDTO task2 = newTask("2");
        task2.setName("tasksk");
        task2.setUserId(user.getId());
        @NotNull final TaskDTO task3 = newTask("3");
        task3.setUserId(user.getId());
        taskClient.createTask(auth, task1);
        taskClient.createTask(auth, task2);
        taskClient.createTask(auth, task3);
        @NotNull final List<TaskDTO> taskList = taskClient.searchTasks(auth, "tasksk");
        Assert.assertEquals(taskList.size(), 2);
        taskClient.deleteTask(auth, task1.getId());
        taskClient.deleteTask(auth, task2.getId());
        taskClient.deleteTask(auth, task3.getId());
    }

    @Test
    public void getTasksByProjectId() {
        @NotNull final ProjectDTO project = newProject("1");
        project.setUserId(user.getId());
        projectClient.createProject(auth, project);
        @NotNull final TaskDTO task1 = newTask("1");
        task1.setUserId(user.getId());
        task1.setProjectId(project.getId());
        @NotNull final TaskDTO task2 = newTask("2");
        task2.setUserId(user.getId());
        task2.setProjectId(project.getId());
        @NotNull final TaskDTO task3 = newTask("3");
        task3.setUserId(user.getId());
        taskClient.createTask(auth, task1);
        taskClient.createTask(auth, task2);
        taskClient.createTask(auth, task3);
        @NotNull final List<TaskDTO> taskList = taskClient.getTasksByProject(auth, project.getId());
        Assert.assertEquals(taskList.size(), 2);
        projectClient.deleteProject(auth, project.getId());
    }

    private TaskDTO newTask(@NotNull final String s) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("TestTask" + s);
        task.setDescription("Description 123");
        task.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        task.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return task;
    }

}
