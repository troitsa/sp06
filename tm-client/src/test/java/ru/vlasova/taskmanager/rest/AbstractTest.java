package ru.vlasova.taskmanager.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.util.Base64Utils;
import ru.vlasova.taskmanager.client.ProjectClient;
import ru.vlasova.taskmanager.client.TaskClient;
import ru.vlasova.taskmanager.client.UserClient;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

public class AbstractTest {

    @NotNull
    final static ProjectClient projectClient = ProjectClient.client("http://localhost:8080");

    @NotNull
    final static TaskClient taskClient = TaskClient.client("http://localhost:8080");

    @NotNull
    final static UserClient userClient = UserClient.client("http://localhost:8080");

    protected static UserDTO newUser(@NotNull final String s) {
        @NotNull final UserDTO user = new UserDTO();
        user.setUsername("userTest" + s);
        user.setPassword("passTest");
        return user;
    }

    protected ProjectDTO newProject(@NotNull final String s) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("TestProject" + s);
        project.setDescription("Description 123");
        project.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        project.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return project;
    }

    protected Date toDateFormat(@NotNull final String date) {
        TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(date);
        Instant i = Instant.from(ta);
        return Date.from(i);
    }

    protected static String login(@NotNull final String username, final String password){
        byte[] encoded = Base64Utils.encode((username + ":" + password).getBytes());
        return "Basic " + new String(encoded);
    }

}
