package ru.vlasova.taskmanager.rest;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.List;

public class ProjectTest extends AbstractTest {

    private static UserDTO user;
    private static String auth;

    @BeforeClass
    public static void before() {
        user = newUser("1");
        userClient.createUser(user);
        auth = login(user.getUsername(), user.getPassword());
    }

    @AfterClass
    public static void after() {
        userClient.deleteUser(auth, user.getId());
    }

    @Test
    public void createProject() {
        @NotNull final ProjectDTO project1 = newProject("1");
        project1.setUserId(user.getId());
        projectClient.createProject(auth, project1);
        Assert.assertNotNull(projectClient.getProject(auth, project1.getId()));
        projectClient.deleteProject(auth, project1.getId());
    }

    @Test
    public void getProjects() {
        @NotNull final ProjectDTO project1 = newProject("1");
        @NotNull final ProjectDTO project2 = newProject("2");
        @NotNull final ProjectDTO project3 = newProject("3");
        project1.setUserId(user.getId());
        project2.setUserId(user.getId());
        project3.setUserId(user.getId());
        projectClient.createProject(auth, project1);
        projectClient.createProject(auth, project2);
        projectClient.createProject(auth, project3);
        @NotNull final List<ProjectDTO> projectList = projectClient.getProjects(auth);
        Assert.assertEquals(projectList.size(), 3);
        projectClient.deleteProject(auth, project1.getId());
        projectClient.deleteProject(auth, project2.getId());
        projectClient.deleteProject(auth, project3.getId());
    }

    @Test
    public void getProject() {
        @NotNull final ProjectDTO project = newProject("1");
        project.setUserId(user.getId());
        projectClient.createProject(auth, project);
        Assert.assertEquals(projectClient.getProject(auth, project.getId()), project);
        projectClient.deleteProject(auth, project.getId());
    }

    @Test
    public void updateProject() {
        @NotNull final ProjectDTO project = newProject("1");
        project.setUserId(user.getId());
        projectClient.createProject(auth, project);
        project.setName("Test-2");
        projectClient.updateProject(auth, project.getId(), project);
        Assert.assertEquals(projectClient.getProject(auth, project.getId()).getName(), project.getName());
        projectClient.deleteProject(auth, project.getId());
    }

    @Test
    public void deleteProject() {
        @NotNull final ProjectDTO project = newProject("1");
        project.setUserId(user.getId());
        projectClient.createProject(auth, project);
        projectClient.deleteProject(auth, project.getId());
        try {
            projectClient.getProject(auth, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("404"));
        }
    }

    @Test
    public void searchProjects() {
        @NotNull final ProjectDTO project1 = newProject("1");
        project1.setUserId(user.getId());
        project1.setName("tasksk");
        @NotNull final ProjectDTO project2 = newProject("2");
        project2.setName("tasksk");
        project2.setUserId(user.getId());
        @NotNull final ProjectDTO project3 = newProject("2");
        project3.setUserId(user.getId());
        projectClient.createProject(auth, project1);
        projectClient.createProject(auth, project2);
        projectClient.createProject(auth, project3);
        @NotNull final List<ProjectDTO> projectList = projectClient.searchProjects(auth, "tasksk");
        Assert.assertEquals(projectList.size(), 2);
        projectClient.deleteProject(auth, project1.getId());
        projectClient.deleteProject(auth, project2.getId());
        projectClient.deleteProject(auth, project3.getId());
    }

}
