package ru.vlasova.taskmanager.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;

import java.util.List;

@FeignClient("project")
public interface ProjectClient {

    static ProjectClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @PostMapping(value = "/rproject")
    ProjectDTO createProject(@RequestHeader("Authorization") String header, ProjectDTO project);

    @GetMapping(value = "/rprojects")
    List<ProjectDTO> getProjects(@RequestHeader("Authorization") String header);

    @GetMapping(value = "/rproject/{id}")
    ProjectDTO getProject(@RequestHeader("Authorization") String header,
                          @PathVariable("id") String id);

    @PutMapping(value = "/rproject/{id}")
    ProjectDTO updateProject(@RequestHeader("Authorization") String header,
                             @PathVariable("id") String id, ProjectDTO project);

    @DeleteMapping(value = "/rproject/{id}")
    void deleteProject(@RequestHeader("Authorization") String header, @PathVariable("id") String id);

    @GetMapping(value = "/rprojects/search/{keyword}")
    List<ProjectDTO> searchProjects(@RequestHeader("Authorization") String header,
                                    @PathVariable(name="keyword") String keyword);

}